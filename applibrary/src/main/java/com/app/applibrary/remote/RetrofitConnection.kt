package com.app.applibrary.remote

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConnection {
    private val BASE_URL = "http://139.59.43.169:3006/"

    fun <S> createServiceWithAuth(serviceClass: Class<S>, interceptor: Interceptor): S {

        /*Log*/
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        logging.level = HttpLoggingInterceptor.Level.BODY


        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor(interceptor)

        val client = httpClient.build()
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(serviceClass)
    }


    /*Header*/
    val interceptor1 = Interceptor { chain ->
        val request = chain.request().newBuilder()
            //.addHeader("Authorization", token)
            // .addHeader("Content-Type", "application/json")
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .build()
        chain.proceed(request)
    }
}