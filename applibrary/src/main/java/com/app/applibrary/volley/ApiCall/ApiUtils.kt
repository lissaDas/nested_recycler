package com.app.applibrary.volley

import android.content.Context
import com.app.europet.app_libraries.appsettings.AppSetttings
import com.app.libraries.volley.constants.ConstantKeys
import java.util.*


fun getContentTypeHeader(context: Context): HashMap<String, String> {

    val headers = HashMap<String, String>()
//   headers.put(NetworkCallConstants.kContentType, NetworkCallConstants.kContentTypeValFormData)
    headers.put(ConstantKeys.kContentType, ConstantKeys.kContentTypeValJSON)

    return headers
}

fun getAuthorizationHeaderTwo(context: Context): HashMap<String, String> {
    val headers = HashMap<String, String>()
    headers.put(ConstantKeys.kAuthorizationKey, AppSetttings(context).getAccessToken())
    headers.put(ConstantKeys.kContentType, ConstantKeys.kContentTypeValJSON)
    return headers
}

fun getAuthorizationHeaderThree(context: Context): HashMap<String, String> {
    val headers = HashMap<String, String>()
    headers.put(ConstantKeys.kAuthorizationKey, AppSetttings(context).getAccessToken())
    headers.put(ConstantKeys.kContentType, ConstantKeys.kContentTypeValJSON)
    return headers
}