package com.app.libraries.volley.constants

class ConstantKeys {

    companion object {
        val kAuthorizationKey: String = "Authorization"
        val kUDID: String = "udId"
        val kContentType: String = "Content-Type"
        val kContentTypeValJSON = "application/json"
        val kContentTypeValUrlEncoded = "application/x-www-form-urlencoded"

    }


}