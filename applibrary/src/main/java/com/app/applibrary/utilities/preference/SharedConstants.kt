package com.app.europet.app_libraries.appsettings

class SharedConstants {
    companion object{
        val accessToken : String = "accessToken"
        val userId : String = "userId"
    }

}