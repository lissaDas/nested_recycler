package com.app.europet.app_libraries.appsettings

import android.content.Context
import com.app.applibrary.tokensecurity.EncryptionUtils
import com.app.applibrary.utilities.preference.*

class AppSetttings(var context: Context?) {

    internal var accessToken: String? = ""
    internal var userId: Int? = -1


    fun getUserId(): Int {
        userId = getInt(context!!, SharedConstants.userId)
        return userId!!
    }

    fun setUserId(userId: Int) {
        putInt(context!!, SharedConstants.userId, userId)
        this.userId = userId
    }


    fun getAccessToken(): String {
        // accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2JpbGVOdW1iZXIiOiIzNDUyMzUzNCIsInVzZXJJZCI6IjVkZDIyODM4YmEyMWI2NmRjZWU4OGNjMiIsImlzU2VydmljZVByb3ZpZGVyIjpmYWxzZX0.0SNVOFlEeK6FUYkBl2MAxFdWhfQQ23YTh_gaE3IeKCk";
        accessToken = EncryptionUtils.decrypt(context, getString(context!!, SharedConstants.accessToken))
        return accessToken!!
    }

    fun setAccessToken(accessToken: String) {
        val encryptedValue = EncryptionUtils.encrypt(context, "jwt $accessToken")
        putString(context!!, SharedConstants.accessToken, encryptedValue)
        this.accessToken = "jwt $accessToken"
    }



}