package com.app.applibrary.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.util.Patterns
import java.util.regex.Pattern


fun isNetworkConnected(context: Context?): Boolean {
    val isConnected: Boolean
    val connectivityManager =
        context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetInfo = connectivityManager
        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
    val activeWIFIInfo = connectivityManager
        .getNetworkInfo(ConnectivityManager.TYPE_WIFI)

    isConnected = activeWIFIInfo!!.isConnected || activeNetInfo!!.isConnected
    return isConnected
}

fun isValidEmail(text: String): Boolean {
    var check = false
    if (text != null && text.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
        check = true
    }
    return check
}

fun isValidName(text: String): Boolean {
    var check = false
    if (text != null && text.isNotBlank() && Pattern.matches("[a-zA-Z0-9. ]+", text)) {
        check = true
    }
    return check
}







