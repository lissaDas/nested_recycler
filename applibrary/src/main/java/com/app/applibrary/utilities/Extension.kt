package com.app.applibrary.utilities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.app.applibrary.R
import com.google.android.material.snackbar.Snackbar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun <T> Context.openActivity(it: Class<T>) {
    var intent = Intent(this, it)
    startActivity(intent)
}

fun <T> Context.openActivityWithBundle(it: Class<T>, bundle: Bundle) {
    var intent = Intent(this, it)
    intent.putExtras(bundle)
    startActivity(intent)
}

fun <T> Context.openFreshActivity(it: Class<T>) {
    var intent = Intent(this, it)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
}

fun <T> Context.openActivityWithClearTop(it: Class<T>) {
    var intent = Intent(this, it)
    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    startActivity(intent)
}

fun <T> Context.openActivityWithSingleTop(it: Class<T>) {
    var intent = Intent(this, it)
    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
    startActivity(intent)
}


fun Any.enableViews(isEnable: Boolean, viewGroup: ViewGroup) {
    for (i in 0 until viewGroup.childCount) {
        val child = viewGroup.getChildAt(i)
        child.isEnabled = isEnable
        if (child is ViewGroup) {
            enableViews(isEnable, child)
        }
    }
}

fun Any.hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Any.getDateFromMilli(milliSeconds: Long, dateFormat: String): String {
    val formatter = SimpleDateFormat(dateFormat)
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    return formatter.format(calendar.time)
}

@Throws(ParseException::class)
fun getTodayOrYesterday(date: String, context: Context): String {
    val dateTime = SimpleDateFormat("dd/MM/yyyy").parse(date)
    val calendar = Calendar.getInstance()
    calendar.time = dateTime
    val today = Calendar.getInstance()
    val yesterday = Calendar.getInstance()
    yesterday.add(Calendar.DATE, -1)
    val todayStr = context.getString(R.string.today)
    val yesterdayStr = context.getString(R.string.yesterday)

    return if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(
            Calendar.DAY_OF_YEAR
        )
    ) {
        todayStr
    } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(
            Calendar.DAY_OF_YEAR
        )
    ) {
        yesterdayStr
    } else {
        date
    }

}

fun Any.openDialPad(content: Activity, mobileNum: String) {
    val callIntent = Intent(Intent.ACTION_DIAL)
    callIntent.data = Uri.parse("tel:" + mobileNum)
    content.startActivity(callIntent)

}

var lastClickTime = 0
fun singleButtonClick(): Boolean {
    if (SystemClock.elapsedRealtime() - lastClickTime < 400) {
        return false
    }

    lastClickTime = SystemClock.elapsedRealtime().toInt()
    return true
}

fun showSnackBar(view: View, text: String, backgroundColor : Int, textColor : Int) {
    var snackbar = Snackbar
        .make(view, text, Snackbar.LENGTH_LONG)
    var snackBarView = snackbar.view
    snackBarView.setBackgroundColor(backgroundColor)
    snackbar.show()
}
fun Any.getName()=this.toString()+"App"