package com.app.applibrary.glide

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.app.applibrary.R
import com.app.applibrary.imageutils.FastBlur
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


class GlideImageLoader(var context: Context) {

    fun load(url: String, imageView: ImageView) {
        val url2 = url.replace("\\/", "/")
        Glide.with(context).load(url2)
            .dontAnimate()
            .apply(requestOptions).into(imageView)
    }


    fun load_Image(url: String, imageView: ImageView) {
        val url2 = url.replace("\\/", "/")
        Glide.with(context).load(url2)
            .dontAnimate()
            .apply(requestOptions1).into(imageView)
    }

    fun loadThumbNail(url: String, imageView: ImageView) {
        val url2 = url.replace("\\/", "/")
        val options = RequestOptions()
        options.placeholder(R.drawable.profile_placeholder)
        options.error(R.drawable.profile_placeholder)
        options.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        Glide.with(context).asBitmap()
            .load(url2)
            .dontAnimate()
            .apply(options.skipMemoryCache(true))
            .into(imageView)
    }

    fun loadThumbNailSquarePlaceholder(url: String, imageView: ImageView) {
        val url2 = url.replace("\\/", "/")
        val options = RequestOptions()
        options.placeholder(R.drawable.squareplaceholder)
        options.error(R.drawable.squareplaceholder)
        options.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        Glide.with(context).asBitmap()
            .load(url2)
            .dontAnimate()
            .apply(options.skipMemoryCache(true))
            .into(imageView)
    }

    fun loadThumbNailImage(url: String, imageView: ImageView) {
        val url2 = url.replace("\\/", "/")
        val options = RequestOptions()
        options.placeholder(R.drawable.image_placeholder)
        options.error(R.drawable.image_placeholder)
        options.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        Glide.with(context).asBitmap()
            .load(url2)
            .dontAnimate()
            .apply(options.skipMemoryCache(true))
            .into(imageView)
    }



    fun loadDp(url: String, imageView: ImageView) {
        try {
            Glide.with(context).load(url).apply(requestOptionsDp).into(imageView)
        } catch (e: Exception) {
        }

    }




    fun loadFile(file: Int, imageView: ImageView) {
        Glide.with(context).load(file).apply(requestOptionsDp).into(imageView)
    }

    fun loadPost(url: String, imageView: ImageView) {
        try {
            Glide.with(context).load(url).apply(requestOptionsDp).into(imageView)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }



    fun loadImageWithBlurBgnd(
        url: String?,
        imageView: ImageView?,
        blurImgView: ImageView
    ) {
        val url2 = url?.replace("\\/", "/")
        val options =
            RequestOptions()
        options.placeholder(R.drawable.image_placeholder)
        options.error(R.drawable.image_placeholder)
        options.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        Glide.with(context).asBitmap()
            .load(url2)
            .apply(options.skipMemoryCache(true))
            .listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val bitmap = Bitmap.createScaledBitmap(
                        resource!!, 250, 250, false
                    )
                    blurImgView.setImageBitmap(FastBlur.doBlur(bitmap, 10, true))
                    return false
                }
            })
            .into(imageView!!)
    }


    companion object {

        val requestOptions1: RequestOptions
            get() {
                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.squareplaceholder)
                requestOptions.error(R.drawable.squareplaceholder)
                return requestOptions
            }
        val requestOptions: RequestOptions
            get() {
                val requestOptions = RequestOptions()
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                requestOptions.placeholder(R.drawable.profile_placeholder)
                requestOptions.error(R.drawable.profile_placeholder)
                return requestOptions
            }



        val requestOptionsDp: RequestOptions
            get() {
                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.profile_placeholder)
                requestOptions.error(R.drawable.profile_placeholder)
                return requestOptions
            }

        val requestOptionsSquarePlaceholder: RequestOptions
            get() {
                val requestOptions = RequestOptions()
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                requestOptions.placeholder(R.drawable.squareplaceholder)
                requestOptions.error(R.drawable.squareplaceholder)
                return requestOptions
            }
        val requestOptionsImageFilePlaceholder: RequestOptions
            get() {
                val requestOptions = RequestOptions()
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                requestOptions.placeholder(R.drawable.image_placeholder)
                requestOptions.error(R.drawable.image_placeholder)
                return requestOptions
            }


    }

}