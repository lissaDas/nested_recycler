package com.app.applibrary.coroutines

import kotlinx.coroutines.*

object CoroutinesUtils
{
    fun mainCoroutineScope(work: suspend (() -> Unit)) = CoroutineScope(Dispatchers.Main).launch { work() }
    fun asyncCoroutineScope(work: suspend (() -> Unit)) = CoroutineScope(Dispatchers.Main).async { work() }
    suspend fun get(url: String,work: suspend (() -> Unit)) = withContext(Dispatchers.IO) {work()}
}