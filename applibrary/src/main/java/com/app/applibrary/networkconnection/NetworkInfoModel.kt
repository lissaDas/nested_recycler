package com.app.applibrary.networkconnection

class NetworkInfoModel(val type: Int, val isConnected: Boolean)
