package com.app.applibrary.view

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.app.applibrary.networkconnection.NetworkConnectionLiveData

abstract class BaseActivity :AppCompatActivity()
{

    private var networkConnectionLiveData: NetworkConnectionLiveData? = null
    abstract fun isNetworking(connection: Boolean)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        setStatusBarColorWhite(this)
    }
    private fun init() {
        isNetworkCheck()
    }

    open fun setStatusBarColorWhite(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.window.statusBarColor = Color.WHITE
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    fun setSystemBarTheme(pActivity: Activity, pIsDark: Boolean) {
        // Fetch the current flags.
        val lFlags = pActivity.window.decorView.systemUiVisibility
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.window.decorView.systemUiVisibility =
            if (pIsDark) lFlags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() else lFlags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
    private fun isNetworkCheck() {

        networkConnectionLiveData = NetworkConnectionLiveData(applicationContext)
        networkConnectionLiveData?.observe(this, Observer { connection ->
            isNetworking(connection.isConnected)
        })

    }
}