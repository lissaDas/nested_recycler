package com.dtod.user


import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("discount")
    var discount: Double = 0.0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("productCode")
    var productCode: String = "",
    @SerializedName("productImage")
    var productImage: String = "",
    @SerializedName("productName")
    var productName: String = "",
    @SerializedName("subcategoryId")
    var subcategoryId: Int = 0
)