package com.dtod.user.mvvm.model

class Login  {
    var mobileNumber: String = ""
    var password: String = ""
    var isServiceProvider: Boolean = false
}

class LoginResponse {

    var error: Boolean = true
    var message: String = ""
    var statusCode: Int = 0

}