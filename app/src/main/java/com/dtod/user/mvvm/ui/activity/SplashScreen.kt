package com.dtod.user.mvvm.ui.activity

import android.os.Build
import android.os.Bundle
import android.os.Handler
import com.app.applibrary.utilities.openActivity
import com.app.applibrary.view.BaseActivity
import com.dtod.user.R

class SplashScreen : BaseActivity() {
    override fun isNetworking(connection: Boolean) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashscreen_layout)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setSystemBarTheme(this,false)
        }
        Handler().postDelayed({
            openActivity(LoginActivity::class.java)
        },2000)
    }

}