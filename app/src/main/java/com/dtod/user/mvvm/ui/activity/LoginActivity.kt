package com.dtod.user.mvvm.ui.activity

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.VectorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.app.applibrary.view.BaseActivity
import com.dtod.user.R
import com.dtod.user.mvvm.ui.fragment.VerifyOtpDialogFragment
import kotlinx.android.synthetic.main.login_layout.*


class LoginActivity : BaseActivity() {
    override fun isNetworking(connection: Boolean) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)
        setListeners()
        setSigninProperties()
        val pop = VerifyOtpDialogFragment()
        val fm = supportFragmentManager
        pop.show(fm, "name")
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getBitmap(context: Context, drawableId: Int): Bitmap? {
        val drawable =
            ContextCompat.getDrawable(context, drawableId)
        return if (drawable is BitmapDrawable) {
            BitmapFactory.decodeResource    (context.getResources(), drawableId)
        } else if (drawable is VectorDrawable) {
            getBitmap((drawable as VectorDrawable?)!!)
        } else {
            throw IllegalArgumentException("unsupported drawable type")
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getBitmap(vectorDrawable: VectorDrawable): Bitmap? {
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        vectorDrawable.draw(canvas)
        return bitmap
    }


    private fun setListeners() {
        signin_text.setOnClickListener {
            setSigninProperties()
        }
        signup_text.setOnClickListener {
            setSignupProperties()
        }
    }

    private fun setSigninProperties() {
        sign_in_bar.visibility= View.VISIBLE
        sign_up_bar.visibility= View.GONE
        signin_text.setTextColor(resources.getColor(R.color.colorPrimary))
        signup_text.setTextColor(resources.getColor(R.color.sign_text))
        label_sign_using.setText(resources.getString(R.string.signin_using))
        forgot_password.setText(resources.getString(R.string.forgot_password))
        signin_signup_button.setText(resources.getString(R.string.sign_in))
    }

    private fun setSignupProperties() {
        sign_up_bar.visibility= View.VISIBLE
        sign_in_bar.visibility= View.GONE
        signup_text.setTextColor(resources.getColor(R.color.colorPrimary))
        signin_text.setTextColor(resources.getColor(R.color.sign_text))
        label_sign_using.setText(resources.getString(R.string.signup_using))
        forgot_password.setText(resources.getString(R.string.signup_continue))
        signin_signup_button.setText(resources.getString(R.string.sign_up))
    }

}