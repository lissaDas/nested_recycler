package com.dtod.user

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.subcategory_item.view.*


class SubcategoryAdapter(
    private var context: Context,
    private val subcategory: List<Subcategory>,
    private var categoryPosition : Int
) : RecyclerView.Adapter<SubcategoryAdapter.SubcategoryViewHolder>() {

    private lateinit var updateSubCategoryInterface: UpdateSubCategoryInterface

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubcategoryViewHolder {
        return SubcategoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.subcategory_item, parent, false),
                    MyCustomEditTextListener())
    }

    override fun getItemCount() = subcategory.size

    override fun onBindViewHolder(holder: SubcategoryViewHolder, position: Int) {
        holder.subcategorytext.setText("Sub: "+subcategory[position].subCategoryName)
        val adapter=ProductAdapter(context,subcategory[position].products,position)
        holder.product_recycler.adapter=adapter
        holder.myCustomEditTextListener.updatePosition(position,adapter)
        holder.price.removeTextChangedListener(holder.myCustomEditTextListener)
        holder.price.setText(subcategory[position].discount.toString())
        holder.price.addTextChangedListener(holder.myCustomEditTextListener)
        adapter.setListener(object : UpdateProductInterface{
            override fun update(subcategoryPosition: Int, productPosition: Int, product: Product) {
                subcategory[subcategoryPosition].products[productPosition]=product
                updateSubCategoryInterface.update(categoryPosition,position,subcategory[position])
            }
        })
    }

    fun setListener(updateSubCategoryInterface: UpdateSubCategoryInterface) {
        this.updateSubCategoryInterface=updateSubCategoryInterface;
    }

    class SubcategoryViewHolder(itemView: View,myCustomEditTextListener: MyCustomEditTextListener) : RecyclerView.ViewHolder(itemView) {
        val price:EditText
        val subcategorytext = itemView.subcategorytext
        val product_recycler = itemView.product_recycler
        var myCustomEditTextListener: MyCustomEditTextListener
        init {
            price=itemView.findViewById<EditText>(R.id.price) as EditText
            this.myCustomEditTextListener = myCustomEditTextListener
        }
    }

     inner class MyCustomEditTextListener : TextWatcher {
         private var position = 0
         private lateinit var adapter: ProductAdapter
         fun updatePosition(
             position: Int,
             adapter: ProductAdapter
         ) {
             this.position = position
             this.adapter = adapter
         }

         override fun beforeTextChanged(
             charSequence: CharSequence,
             i: Int,
             i2: Int,
             i3: Int
         ) {
             // no op
         }

         override fun onTextChanged(
             charSequence: CharSequence,
             i: Int,
             i2: Int,
             i3: Int
         ) {
             if(charSequence.toString().length!=0){
                 if(charSequence.toString().toDouble()<=100.0){
                     Log.e("pos", "onTextChanged: "+position )
                     subcategory[position].discount=charSequence.toString().toDouble()
                     subcategory[position].products.forEachIndexed { index, product ->
                         subcategory[position].products[index].discount=charSequence.toString().toDouble()
                     }
                     updateSubCategoryInterface.update(categoryPosition,position,subcategory[position])
                     adapter.updateData(subcategory[position].products)
                 }
             }else{
                 subcategory[position].discount=0.0
                 subcategory[position].products.forEachIndexed { index, product ->
                     subcategory[position].products[index].discount=0.0
                 }
                 updateSubCategoryInterface.update(categoryPosition,position,subcategory[position])
                 adapter.updateData(subcategory[position].products)
             }

         }

         override fun afterTextChanged(editable: Editable) {
             // no op
         }
     }
}