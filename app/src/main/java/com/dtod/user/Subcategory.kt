package com.dtod.user


import com.google.gson.annotations.SerializedName

data class Subcategory(
    @SerializedName("activeStatus")
    var activeStatus: Int = 0,
    @SerializedName("categoryId")
    var categoryId: Int = 0,
    @SerializedName("discount")
    var discount: Double = 0.0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("products")
    var products: ArrayList<Product> = arrayListOf(),
    @SerializedName("subCategoryDescription")
    var subCategoryDescription: String = "",
    @SerializedName("subCategoryImage")
    var subCategoryImage: String = "",
    @SerializedName("subCategoryName")
    var subCategoryName: String = ""
)