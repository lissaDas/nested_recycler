package com.dtod.user


import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @SerializedName("category")
    var category: List<Category> = listOf(),
    @SerializedName("error")
    var error: Boolean = false,
    @SerializedName("message")
    var message: String = "",
    @SerializedName("statusCode")
    var statusCode: Int = 0
)