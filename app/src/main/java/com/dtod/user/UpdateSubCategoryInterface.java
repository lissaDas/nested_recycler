package com.dtod.user;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface UpdateSubCategoryInterface {
    void update(int categoryPosition,int subCategoryPosition, @NotNull Subcategory subcategory);
}
