package com.dtod.user

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sample.*
import org.json.JSONObject

class Sample : AppCompatActivity() {
    private lateinit var adapter: CategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val jsonfile: String =
            applicationContext.assets.open("sample.json").bufferedReader().use { it.readText() }
        val jsonObject = JSONObject(jsonfile)
        val responseModel: ResponseModel = Gson().fromJson(
            jsonObject.toString(),
            ResponseModel::class.java
        )
        adapter=CategoryAdapter(this,responseModel.category)
        categoryRecycler.adapter=adapter
        button.setOnClickListener {
            Log.e("json", "onCreate: "+Gson().toJson(adapter.getAllList()))
        }
    }


}