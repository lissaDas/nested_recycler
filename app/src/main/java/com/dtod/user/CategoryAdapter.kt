package com.dtod.user

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryAdapter(
    var context: Context,
    var category: List<Category>
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.category_item, parent, false)
        )
    }

    override fun getItemCount() = category.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.text.setText(category[position].categoryName)
        val adapter=SubcategoryAdapter(context,category[position].subcategory,position)
        adapter.setListener(object : UpdateSubCategoryInterface{
            override fun update(categoryPosition: Int,subCategoryPosition: Int, subcategory: Subcategory) {
                category[categoryPosition].subcategory[subCategoryPosition]=subcategory
            }
        })
        holder.subcategory_recycler.adapter=adapter
    }

    fun getAllList(): List<Category> {
        return category
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text=itemView.categorytext
        val subcategory_recycler=itemView.subcategory_recycler
    }
}