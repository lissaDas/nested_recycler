package com.dtod.user


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("activeStatus")
    var activeStatus: Int = 0,
    @SerializedName("categoryDescription")
    var categoryDescription: String = "",
    @SerializedName("categoryImage")
    var categoryImage: String = "",
    @SerializedName("categoryName")
    var categoryName: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("subcategory")
    var subcategory: ArrayList<Subcategory> = arrayListOf()
)