package com.dtod.user

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.product_item.view.*
import java.util.*

class ProductAdapter(
    var context: Context,
    var products: List<Product>,
    var subcategoryPosition : Int
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {


    private lateinit var updateProductInterface: UpdateProductInterface

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.product_item, parent, false),
            MyCustomEditTextListener()
        )
    }

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.product_text.setText("Pro: "+products[position].productName)
        holder.myCustomEditTextListener.updatePosition(position)
        holder.price.removeTextChangedListener(holder.myCustomEditTextListener)
        holder.price.setText(products[position].discount.toString())
        holder.price.addTextChangedListener(holder.myCustomEditTextListener)
    }

    fun updateData(products: List<Product>) {
        this.products=products
        notifyDataSetChanged()
    }

    fun setListener(updateProductInterface: UpdateProductInterface) {
        this.updateProductInterface=updateProductInterface
    }

    class ProductViewHolder(itemView: View,myCustomEditTextListener: MyCustomEditTextListener) : RecyclerView.ViewHolder(itemView) {
        val price: EditText
        val product_text=itemView.product_text
        var myCustomEditTextListener: MyCustomEditTextListener
        init {
            price=itemView.findViewById<EditText>(R.id.price) as EditText
            this.myCustomEditTextListener = myCustomEditTextListener
        }
    }

    inner class MyCustomEditTextListener : TextWatcher {
        private var position = 0
        fun updatePosition(
            position: Int
        ) {
            this.position = position
        }

        override fun beforeTextChanged(
            charSequence: CharSequence,
            i: Int,
            i2: Int,
            i3: Int
        ) {
            // no op
        }

        override fun onTextChanged(
            charSequence: CharSequence,
            i: Int,
            i2: Int,
            i3: Int
        ) {
            if(charSequence.toString().toString().length!=0) {
                if(charSequence.toString().toDouble()<=100.0) {

                    products[position].discount = charSequence.toString().toDouble()
                    updateProductInterface.update(subcategoryPosition, position, products[position])
                }
            }else{
                products[position].discount = 0.0
                updateProductInterface.update(subcategoryPosition, position, products[position])
            }

        }

        override fun afterTextChanged(editable: Editable) {
            // no op
        }
    }

}