package com.dtod.user;

import org.jetbrains.annotations.NotNull;

public interface UpdateProductInterface {
    void update(int subcategoryPosition, int productPosition, @NotNull Product product);
}
